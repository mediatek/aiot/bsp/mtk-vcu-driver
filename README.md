# mtk-vcu-driver

VCU is a proprietary module that allow user space program to communicate with the other proprietary hardware driver in Mediatek SoC.

This repo is out-of-tree kernel driver of VCU.


## Branches

The kernel driver is compatible with [mtk-v5.15-dev](https://gitlab.com/mediatek/aiot/bsp/linux/-/tree/mtk-v5.15-dev) and it is still under development and subject to design changes.
Currently, different SoC Platform requires different branches of VCU driver.
Each branch exposes different userspace interfaces and are not compatible to each other.

- The main branch is unused.
- For Genio 1200 and Genio 700 platform: use `mt8395` branch.
- For Genio 350 platform: use `mt8167` branch.


